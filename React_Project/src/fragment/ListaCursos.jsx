import React from 'react';
import '../css/listaTareas.css'

const ListaCursos = () => {
  return (

    <div className='col-12' id='usuario'><br />
      <center><h1>LISTA DE CURSOS</h1></center>
      <br />
      <div className='tabla'>
      <table className="table table-bordered">
        <thead className="thead-dark">
          <tr>
            <th>NOMBRE DEL CURSO</th>
            <th>PERIODO</th>
            <th>VER</th>
          </tr>
        </thead>
        <tbody style={{ backgroundColor: '#ffffff' }}>
        <tr>
              <td>5to Ciclo</td>
              <td>ABR23-SEP23</td>
              <td>
              <a className='btn btn-primary' href={'/TareasCreadas'} role="button" >Ver Curso</a>
              </td>
            </tr>
            <tr>
              <td>4to Ciclo</td>
              <td>ABR23-SEP23</td>
              <td>
              <a className='btn btn-primary' href={'/TareasCreadas'} role="button" >Ver Curso</a>
              </td>
            </tr>
        </tbody>
      </table>
      </div>
    </div>

  );
};

export default ListaCursos;