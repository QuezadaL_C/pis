const Footer = () => {
    return (
    <div className="copyright text-center my-auto">
        <span>Copyright © Your Website 2021</span>
    </div>
    );
}

export default Footer;