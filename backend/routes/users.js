var express = require('express');
var router = express.Router();
const { body, validationResult } = require('express-validator');
const RolController = require('../controller/RolController');
const EstudianteController = require('../controller/EstudianteController');
const ProfesorController = require('../controller/ProfesorController');
var rolController = new RolController();
var estudianteController = new EstudianteController();
var profesorController = new ProfesorController();
const CuentaController = require('../controller/CuentaController');
var cuentaController = new CuentaController();
const MateriaController = require('../controller/MateriaController');
var materiaController = new MateriaController();
const TareaController = require('../controller/TareaController');
var tareaController = new TareaController();
const CursaController = require('../controller/CursaController');
var cursaController = new CursaController();
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.json({ "version": "1.0", "name": "auto" });
});

router.get('/roles', rolController.listar);

router.post('/estudiante/modificar', estudianteController.modificar);

router.post('/estudiante/guardar', [
  body('apellidos', 'Ingrese su apellido').exists(),
  body('nombres', 'Ingrese algun dato').exists()
], estudianteController.guardar);

router.post('/cursa/guardar', cursaController.guardar);

router.post('/profesor/guardar', [
  body('apellidos', 'Ingrese su apellido').exists(),
  body('nombres', 'Ingrese algun dato').exists()
], profesorController.guardar);

router.get('/cursa/external', cursaController.extraerIdCursa);
router.get('/cursa/tareas', cursaController.extraerNombreTarea);
router.get('/estudiante', estudianteController.listar);
router.post('/sesion', cuentaController.sesion);
router.post('/tarea/guardar', tareaController.guardar);
router.get('/estudiante/obtener', estudianteController.obtener);
router.get('/cursa/listar', cursaController.extraerTodo);
router.get('/', function (req, res, next) {
  res.json({ "version": "1.0", "name": "auto" });
});
router.get('/practicas/listar',tareaController.listar);
module.exports = router;