'use strict';
const { DataTypes } = require('sequelize');
module.exports = (sequelize,DataTypes) => {
  const ciclo = sequelize.define('ciclo', {
    id_materia: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
    paralelo: {
      type: DataTypes.STRING(10),
      allowNull: false
    }
  }, {
    freezeTableName: true
  });

  ciclo.associate = function(models) {
    // Asociación con el modelo Materia
    ciclo.belongsTo(models.materia, {
      foreignKey: 'id_materia',
      as: 'materia'
    });
  };

  return ciclo;
};
